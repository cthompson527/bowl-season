package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	uuid "github.com/satori/go.uuid"
)

func main() {
	env := os.Getenv("APP_ENV")
	if env == "" {
		log.Fatal("you must set APP_ENV variable")
	} else if env == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	if _, err := os.Stat("env.json"); !os.IsNotExist(err) {
		err := setEnvFromJSON("env.json", env)
		checkErr(err)
	}

	db, err := getDB()
	checkErr(err)
	defer db.Close()

	store := cookie.NewStore([]byte(os.Getenv("SESSION_SECRET")))

	r := gin.Default()
	r.Use(sessions.Sessions("mysession", store))
	r.Static("/static", "./assets")
	r.SetFuncMap(template.FuncMap{
		"modIsZero": modIsZero,
		"plus":      plus,
		"gThan":     greaterThan,
	})
	r.LoadHTMLGlob("templates/*")
	r.StaticFile("favicon.ico", "./assets/favicon.ico")
	r.GET("/", indexHandler)
	r.GET("/about", aboutHandler)
	r.GET("/picks", picksHandler)
	r.GET("/league", leagueHandler)
	r.GET("/compare", compareHandler)
	r.GET("/edit/:user_id", editGetHandler)
	r.POST("/submit/:user_id", submitPostHandler)
	r.GET("/login", login)
	r.POST("/login", loginSubmit)
	admin := r.Group("/admin")
	admin.Use(AuthRequired())
	{
		admin.GET("/", editGamesHandler)
		admin.POST("/submit", editGamesSubmission)
	}
	r.Run()
}

// PickID is used to determine if the user has already picked. This is used in the Edit page but not shown.
type PickID struct {
	ID     string
	UserID string
}

// Bowl is the bowl's information. This is shown on the Index page.
type Bowl struct {
	BowlName   string
	HomeTeam   string
	HomeConf   string
	HomeRank   *int32
	AwayTeam   string
	AwayConf   string
	AwayRank   *int32
	HomeScore  *int32
	AwayScore  *int32
	GamePoints *int32
	IsFinished bool
}

// PickBowl is each option of bowl to pick. This is shown on the Edit page
type PickBowl struct {
	BowlID     string
	BowlName   string
	GamePoints *int32
	HomeID     string
	AwayID     string
	HomeTeam   string
	AwayTeam   string
	HomeRank   *int32
	AwayRank   *int32
}

// Pick is each user's pick and the pick information. This is show on the Picks page
type Pick struct {
	FirstName  string
	LastName   string
	PickTeam   string
	HomeTeam   string
	AwayTeam   string
	PickRank   *int32
	HomeRank   *int32
	AwayRank   *int32
	HomeScore  *int32
	AwayScore  *int32
	GamePoints int32
	IsFinished bool
}

// User is each user and their collective points. This is shown on the League page
type User struct {
	Name              string
	TotalCorrect      int32
	GamePointsCorrect int32
	ScoreDiff         int32
}

// Compare is used to display all the differences between two users' picks
type Compare struct {
	BowlName   string
	HomeTeam   string
	HomeScore  *int32
	AwayTeam   string
	AwayScore  *int32
	P1Pick     string
	P2Pick     string
	GamePoints int32
}

// UserDropdown is used to create the user dropdown selection list
type UserDropdown struct {
	ID        string
	FirstName string
	LastName  string
}

// EditGames is used to modify the games after the conclusion
type EditGames struct {
	ID         string
	BowlName   string
	GamePoints int32
	HomeTeam   string
	HomeID     string
	AwayTeam   string
	AwayID     string
}

func indexHandler(c *gin.Context) {
	var bowls []*Bowl
	q := "SELECT g.bowl_name, ht.team_name AS home_team, ht.conference AS home_conf, ht.ranking AS home_rank, at.team_name AS away_team, at.conference AS away_conf, at.ranking AS away_rank, g.home_score, g.away_score, g.game_points, g.is_finished FROM games g JOIN teams ht ON g.home_team = ht.id JOIN teams at ON g.away_team = at.id;"
	Db.Raw(q).Scan(&bowls)

	c.HTML(http.StatusOK, "index.gohtml", gin.H{"Bowls": bowls})
}

func picksHandler(c *gin.Context) {
	openTime := time.Date(2018, 12, 15, 6, 0, 0, 0, time.UTC)
	if time.Now().Before(openTime) {
		c.HTML(http.StatusOK, "too_early.gohtml", nil)
		return
	}

	var picks []*Pick
	q := "SELECT u.first_name, u.last_name, pt.team_name as pick_team, ht.team_name as home_team, at.team_name as away_team, pt.ranking as pick_rank, ht.ranking as home_rank, at.ranking as away_rank, g.home_score, g.away_score, g.game_points, g.is_finished FROM picks p JOIN users u ON p.user = u.id JOIN games g ON p.game = g.id JOIN teams pt ON p.team = pt.id JOIN teams ht ON g.home_team = ht.id JOIN teams at ON g.away_team = at.id;"
	Db.Raw(q).Scan(&picks)

	c.HTML(http.StatusOK, "picks.gohtml", gin.H{"Picks": picks})
}

func leagueHandler(c *gin.Context) {
	var picks []*Pick
	q := "SELECT u.first_name, u.last_name, pt.team_name as pick_team, ht.team_name as home_team, at.team_name as away_team, pt.ranking as pick_rank, ht.ranking as home_rank, at.ranking as away_rank, g.home_score, g.away_score, g.game_points, g.is_finished FROM picks p JOIN users u ON p.user = u.id JOIN games g ON p.game = g.id JOIN teams pt ON p.team = pt.id JOIN teams ht ON g.home_team = ht.id JOIN teams at ON g.away_team = at.id;"
	Db.Raw(q).Scan(&picks)

	users := make(map[string]*User)

	for _, pick := range picks {
		if !pick.IsFinished {
			continue
		}

		fullName := fmt.Sprintf("%s %s", pick.FirstName, pick.LastName)
		if users[fullName] == nil {
			users[fullName] = &User{fullName, 0, 0, 0}
		}

		var winnerTeam string
		var scoreDiff int32

		if *pick.HomeScore > *pick.AwayScore {
			winnerTeam = pick.HomeTeam
			scoreDiff = *pick.HomeScore - *pick.AwayScore
		} else {
			winnerTeam = pick.AwayTeam
			scoreDiff = *pick.AwayScore - *pick.HomeScore
		}

		if pick.PickTeam == winnerTeam {
			users[fullName].TotalCorrect++
			users[fullName].GamePointsCorrect += pick.GamePoints
			users[fullName].ScoreDiff += scoreDiff
		} else {
			users[fullName].ScoreDiff -= scoreDiff
		}

	}

	c.HTML(http.StatusOK, "league.gohtml", gin.H{"Users": users})
}

func compareHandler(c *gin.Context) {
	var users []*UserDropdown
	var differences []*Compare
	var p1Name string
	var p2Name string
	Db.Raw(`SELECT * FROM users ORDER BY first_name;`).Scan(&users)

	userOne, existsOne := c.GetQuery("user_one")
	userTwo, existsTwo := c.GetQuery("user_two")
	if existsOne && existsTwo {
		Db.Raw(`SELECT g.bowl_name, ht.team_name AS home_team, g.home_score, "at".team_name AS away_team, g.away_score, p1t.team_name AS p1_pick, p2t.team_name AS p2_pick, g.game_points FROM games g JOIN teams ht ON g.home_team = ht.id JOIN teams "at" ON g.away_team = "at".id JOIN picks p1 ON g.id = p1.game AND p1."user" = ? JOIN teams p1t ON p1.team = p1t.id JOIN picks p2 ON g.id = p2.game AND p2."user" = ? JOIN teams p2t ON p2.team = p2t.id WHERE p1.team != p2.team;`, userOne, userTwo).Scan(&differences)
		for _, user := range users {
			if user.ID == userOne {
				p1Name = user.FirstName
			} else if user.ID == userTwo {
				p2Name = user.FirstName
			}
		}
	}

	c.HTML(http.StatusOK, "compare.gohtml", gin.H{"Games": differences, "Users": users, "PlayerOne": p1Name, "PlayerTwo": p2Name})
}

func editGetHandler(c *gin.Context) {
	closeTime := time.Date(2018, 12, 15, 6, 0, 0, 0, time.UTC)
	if time.Now().After(closeTime) {
		c.HTML(http.StatusOK, "too_late.gohtml", nil)
		return
	}

	userID := c.Param("user_id")
	var pickIDs []*PickID
	Db.Raw(`SELECT * FROM picks WHERE "user" = ?;`, userID).Scan(&pickIDs)
	if len(pickIDs) > 0 {
		c.HTML(http.StatusOK, "already_selected.gohtml", nil)
		return
	}

	var bowls []*PickBowl
	q := "SELECT g.id AS bowl_id, g.bowl_name, g.game_points, ht.id AS home_id, at.id AS away_id, ht.team_name AS home_team, at.team_name AS away_team, ht.ranking AS home_rank, at.ranking AS away_rank FROM games g JOIN teams ht ON g.home_team = ht.id JOIN teams at ON g.away_team = at.id ORDER BY game_points;"
	Db.Raw(q).Scan(&bowls)

	c.HTML(http.StatusOK, "edit.gohtml", gin.H{"Bowls": bowls, "UserID": userID})
}

func submitPostHandler(c *gin.Context) {
	uid := c.Param("user_id")
	picks := c.PostFormMap("picks")
	if len(picks) < 39 {
		c.HTML(http.StatusOK, "missing_selections.gohtml", nil)
		return
	}

	var bowls []*PickBowl
	q := "SELECT g.id AS bowl_id, ht.id AS home_id, at.id AS away_id, ht.team_name AS home_team, at.team_name AS away_team, ht.ranking AS home_rank, at.ranking AS away_rank FROM games g JOIN teams ht ON g.home_team = ht.id JOIN teams at ON g.away_team = at.id;"
	Db.Raw(q).Scan(&bowls)

	for game, team := range picks {
		id, err := uuid.NewV4()
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, fmt.Errorf("could not create an uuid: %v", err))
			return
		}

		if err := Db.Exec("INSERT INTO picks VALUES (?, ?, ?, ?);", id, uid, game, team).Error; err != nil {
			c.AbortWithError(http.StatusInternalServerError, fmt.Errorf("could not insert into db: %v", err))
			return
		}
	}

	c.Redirect(http.StatusFound, "/?submitted=true")
}

func aboutHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "about.gohtml", nil)
}

func login(c *gin.Context) {
	c.HTML(http.StatusOK, "login.gohtml", nil)
}

// AuthRequired is the middleware function used for analyzing the session string
func AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		user := session.Get("user")
		if user == nil {
			c.Redirect(http.StatusFound, "/login")
		} else {
			c.Next()
		}
	}
}

func editGamesHandler(c *gin.Context) {
	var games []*EditGames
	q := "SELECT g.id, g.bowl_name, g.game_points, h.team_name AS home_team, h.id AS home_id, a.team_name AS away_team, a.id AS away_id FROM games g JOIN teams h ON g.home_team = h.id JOIN teams a ON g.away_team = a.id WHERE g.is_finished = FALSE ORDER BY g.game_points;"
	Db.Raw(q).Scan(&games)

	c.HTML(http.StatusOK, "edit_games.gohtml", gin.H{"Bowls": games})
}

func editGamesSubmission(c *gin.Context) {
	var update string
	picks := c.PostFormMap("picks")

	for game, score := range picks {
		if score == "" {
			continue
		}
		bowlTeam := strings.Split(game, "::")
		if bowlTeam[1] == "Home" {
			update = "UPDATE games SET home_score = ?, is_finished = TRUE WHERE id = ?;"
		} else {
			update = "UPDATE games SET away_score = ?, is_finished = TRUE WHERE id = ?;"
		}

		if err := Db.Exec(update, score, bowlTeam[0]).Error; err != nil {
			c.JSON(http.StatusInternalServerError, "could not update scores")
		}
	}
	c.Redirect(http.StatusFound, "/?submitted=true")
}

func loginSubmit(c *gin.Context) {
	session := sessions.Default(c)
	username := c.PostForm("username")
	password := c.PostForm("password")

	if strings.Trim(username, " ") == "" || strings.Trim(password, " ") == "" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Parameters can't be empty"})
		return
	}
	if username == os.Getenv("SECRET_USERNAME") && password == os.Getenv("SECRET_PASSWORD") {
		session.Set("user", username)
		session.Options(sessions.Options{
			MaxAge: 60 * 15, // 15 mins
		})
		err := session.Save()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to generate session token"})
		} else {
			c.Redirect(http.StatusFound, "/")
		}
	} else {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed"})
	}
}

func logout(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")
	if user == nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid session token"})
	} else {
		log.Println(user)
		session.Delete("user")
		session.Save()
		c.JSON(http.StatusOK, gin.H{"message": "Successfully logged out"})
	}
}

func modIsZero(num1, num2 int) bool {
	return num1%num2 == 0
}

func plus(a, b int) int {
	return a + b
}

func greaterThan(num1, num2 *int32) bool {
	return *num1 > *num2
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
