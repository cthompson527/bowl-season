package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type environments struct {
	Testing     environment `json:"testing"`
	Development environment `json:"development"`
	Production  environment `json:"production"`
}

type environment struct {
	Host          string `json:"PG_HOST"`
	Port          string `json:"PG_PORT"`
	Name          string `json:"PG_DBNAME"`
	Username      string `json:"PG_USERNAME"`
	Password      string `json:"PG_PASSWORD"`
	SSLMode       string `json:"SSLMODE"`
	SessionSecret string `json:"SESSION_SECRET"`
	SecretUser    string `json:"SECRET_USERNAME"`
	SecretPass    string `json:"SECRET_PASSWORD"`
}

// setEnvFromJSON will set the environment variables based
// on two parameters, the path to the json env file and the env to set
func setEnvFromJSON(path string, env string) error {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	var envs environments
	err = json.Unmarshal(b, &envs)
	if err != nil {
		return err
	}

	var environ environment

	switch env {
	case "development":
		environ = envs.Development
	case "production":
		environ = envs.Production
	case "testing":
		environ = envs.Testing
	}

	return setEnvironmentVariables(environ)
}

func setEnvironmentVariables(env environment) error {
	if err := os.Setenv("PG_HOST", env.Host); err != nil {
		return err
	}
	if err := os.Setenv("PG_PORT", env.Port); err != nil {
		return err
	}
	if err := os.Setenv("PG_DBNAME", env.Name); err != nil {
		return err
	}
	if err := os.Setenv("PG_USERNAME", env.Username); err != nil {
		return err
	}
	if err := os.Setenv("PG_PASSWORD", env.Password); err != nil {
		return err
	}
	if err := os.Setenv("SSLMODE", env.SSLMode); err != nil {
		return err
	}
	if err := os.Setenv("SESSION_SECRET", env.SessionSecret); err != nil {
		return err
	}
	if err := os.Setenv("SECRET_USERNAME", env.SecretUser); err != nil {
		return err
	}
	if err := os.Setenv("SECRET_PASSWORD", env.SecretPass); err != nil {
		return err
	}
	return nil
}
