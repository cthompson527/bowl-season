#! /bin/bash
if [ "$CREATE_DB" = "Y" ]; then
  dbmate create
fi

if [ "$SEED_DB" = "Y" ]; then
  dbmate up
fi

exec /main
