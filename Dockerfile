FROM registry.access.redhat.com/devtools/go-toolset-1.10-rhel7 AS build

USER root
ARG GOPATH=/opt/app-root/src/go
WORKDIR $GOPATH/src/gitlab.com/cthompson527/bowl-season
COPY . .
RUN export PATH=$PATH:/opt/rh/go-toolset-1.10/root/usr/bin && \
  go get -d -v && \
  go build -o main

FROM registry.access.redhat.com/rhel7-atomic AS run
COPY --from=build /opt/app-root/src/go/src/gitlab.com/cthompson527/bowl-season/main .
COPY ./templates ./templates
COPY ./assets ./assets
COPY ./db ./db
COPY ./run.sh ./

RUN curl -fsSL -o /usr/local/bin/dbmate https://github.com/amacneil/dbmate/releases/download/v1.4.1/dbmate-linux-amd64
RUN chmod +x /usr/local/bin/dbmate

EXPOSE 8080
USER 1001

CMD [ "/run.sh" ]
