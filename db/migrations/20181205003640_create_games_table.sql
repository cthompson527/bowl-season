-- migrate:up
CREATE TABLE games (
  id uuid PRIMARY KEY,
  home_team uuid REFERENCES teams(id),
  away_team uuid REFERENCES teams(id),
  bowl_name text,
  home_score integer,
  away_score integer,
  game_points integer,
  is_finished boolean
);

-- migrate:down
DROP TABLE games;
