-- migrate:up
CREATE TABLE picks (
  id uuid PRIMARY KEY,
  "user" uuid REFERENCES users(id),
  game uuid REFERENCES games(id),
  team uuid REFERENCES teams(id),
  CONSTRAINT unq_user_game UNIQUE("user", game)
);


-- migrate:down
DROP TABLE picks;
