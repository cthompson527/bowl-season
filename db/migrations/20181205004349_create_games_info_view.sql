-- migrate:up
CREATE VIEW games_info AS
  SELECT g.bowl_name,
    ht.team_name AS home_team,
    ht.conference AS home_conf,
    ht.ranking AS home_rank,
    at.team_name AS away_team,
    at.conference AS away_conf,
    at.ranking AS away_rank,
    g.home_score,
    g.away_score,
    g.game_points,
    g.is_finished
   FROM games g
     JOIN teams ht ON g.home_team = ht.id
     JOIN teams at ON g.away_team = at.id;

-- migrate:down
DROP VIEW games_info;

