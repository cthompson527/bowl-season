-- migrate:up
CREATE VIEW pick_info AS
  SELECT u.first_name, u.last_name, pt.team_name as picked_team, ht.team_name as home_team, at.team_name as away_team, g.home_score, g.away_score, g.game_points, g.is_finished 
    FROM picks p 
    JOIN users u ON p.user = u.id
    JOIN games g ON p.game = g.id
    JOIN teams pt ON p.team = pt.id
    JOIN teams ht ON g.home_team = ht.id
    JOIN teams at ON g.away_team = at.id;

-- migrate:down
DROP VIEW pick_info;
