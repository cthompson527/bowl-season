-- migrate:up
CREATE TABLE users (
  id uuid PRIMARY KEY,
  first_name text,
  last_name text
);

-- migrate:down
DROP TABLE users;
