-- migrate:up
CREATE TABLE teams (
    id uuid PRIMARY KEY,
    conference text,
    team_name text,
    ranking integer
);

-- migrate:down
DROP TABLE teams;
