package main

import (
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
)

// Db is the exported DB variable to be used throughout the application
var Db *gorm.DB

// getDB setups the DB and saves it to the exported Db variable
func getDB() (*gorm.DB, error) {
	host := os.Getenv("PG_HOST")
	port := os.Getenv("PG_PORT")
	name := os.Getenv("PG_DBNAME")
	username := os.Getenv("PG_USERNAME")
	password := os.Getenv("PG_PASSWORD")
	environment := os.Getenv("APP_ENV")
	sslMode := os.Getenv("SSLMODE")
	var connectionString string
	if username == "" {
		connectionString = fmt.Sprintf("host=%s port=%s dbname=%s sslmode=%s", host, port, name, sslMode)
	} else {
		connectionString = fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=%s", host, port, name, username, password, sslMode)
	}

	log.Print(connectionString)

	db, err := gorm.Open("postgres", connectionString)
	if err != nil {
		return nil, fmt.Errorf("error opening DB: %v", err)
	}

	if environment == "development" {
		db.LogMode(true)
	}

	Db = db
	return Db, nil
}
